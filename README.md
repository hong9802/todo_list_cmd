# ToDo_List_cmd

## How to use?
```bash
$ python3 todo.py -a your_date your_work
```
ex) python3 todo.py -a 2019-06-15 I have to go my grandfather's house
  
ex) python3 todo.py -d
## It has option
* -a : append your work & date
* -s : starting...(use init.d with looping coodinate)
* -d : delete your date
etc...