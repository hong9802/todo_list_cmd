import sqlite3
import subprocess
import sys
from datetime import datetime

#python3 todo.py -a 2019-06-15 doing my workblablabla

def make_string(): #make a work sentence...
    s = ""
    for i in range(3, len(sys.argv), 1):
        s += sys.argv[i] + " "
    return s

if (__name__ == "__main__"):
    conn = sqlite3.connect("ToDo_List.db")
    curs = conn.cursor()
    try:
        curs.execute("SELECT * FROM todolist")
    except sqlite3.OperationalError as e:
        curs.execute("create table todolist(date, work)")
    if(sys.argv[1] == "-a"): #append your date & work
        insert_data = "insert into todolist(date, work) values (?, ?)"
        curs.execute(insert_data, [sys.argv[2], make_string()])
        print("Done~")
    elif(sys.argv[1] == "-s"): #using init.d with looping coodinate
        times = curs.fetchall()
        for i in range(0, len(times), 1):
            if(times[i][0] == datetime.now().strftime("%Y-%m-%d")):
                subprocess.Popen(['notify-send', times[i][1]])
                break
    elif(sys.argv[1] == "-d"): #delete your date & work...
        times = curs.fetchall()
        del_data = "DELETE FROM todolist WHERE date = '" + sys.argv[2] + "'"
        for i in range(0, len(times), 1):
            if(times[i][0] == sys.argv[2]):
                curs.execute(del_data)
                print("Done~~")
                break
    conn.commit()
    conn.close()